Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    get 'oka', to: 'devise/sessions#new'
  end
  resources :works do
    delete :destroy_img, on: :member
  end
  resources :categories, path: "" do
    delete :destroy_img, on: :member
  end
  root "categories#index"

end
