class Category < ApplicationRecord
  include Image
  include Slugable
  has_ancestry
  has_one_attached :image
  has_many :works, dependent: :destroy
  validates :name, :image, presence: true
end
