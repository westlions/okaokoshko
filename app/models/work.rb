class Work < ApplicationRecord
  include Image
  include Slugable
  has_one_attached :image
  has_many_attached :images
  belongs_to :category
  validates :name, :image, :images, presence: true
end
