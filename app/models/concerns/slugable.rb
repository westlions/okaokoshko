module Slugable
  extend ActiveSupport::Concern

  included do
    before_save :set_slug
    validates   :slug, uniqueness: true
  end

  def to_param
    return unless persisted?

    slug
  end

  private

  def set_slug
    self.slug = name.to_s.parameterize
  end
end
