module Image
  def thumbnail
    return image.variant(resize: '300x').processed if image.attached?
  end

  def standart_images
    return images.map { |image| image.variant(resize: '1000x').processed.processed } if images.attached?
  end
end
