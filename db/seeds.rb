User.delete_all
Category.delete_all
Work.delete_all

Dir.glob(Rails.root.join('storage', '**', '*').to_s).sort_by(&:length).reverse.each do |x|
  if File.directory?(x) && Dir.empty?(x)
    Dir.rmdir(x)
  end
end

some_date = rand(1...10).days.ago

User.create!(
  email: 'test@test.com',
  password: 'Zxasqw12',
  password_confirmation: 'Zxasqw12',
  created_at: Time.now
)

User.create!(
  email: 'Forzavale9@gmail.com',
  password: 'OkaOkita2020',
  password_confirmation: 'OkaOkita2020',
  created_at: Time.now
)

category_name = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Integer nec odio.", "Praesent libero.", "Sed cursus ante dapibus diam.", "Sed nisi.", "Nulla quis sem at nibh elementum imperdiet.", "Duis sagittis ipsum.", "Praesent mauris.", "Fusce nec tellus sed augue semper porta.", "Mauris massa.", "Vestibulum lacinia arcu eget nulla.", "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.", "Curabitur sodales ligula in libero.", "Sed dignissim lacinia nunc.", "Curabitur tortor.", "Pellentesque nibh.", "Aenean quam.", "In scelerisque sem at dolor.", "Maecenas mattis.", "Sed convallis tristique sem.", "Proin ut ligula vel nunc egestas porttitor.", "Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa.", "Fusce ac turpis quis ligula lacinia aliquet.", "Mauris ipsum.", "Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.", "Quisque volutpat condimentum velit.", "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.", "Nam nec ante.", "Sed lacinia, urna non tincidunt mattis, tortor neque adipiscing diam, a cursus ipsum ante quis turpis.", "Nulla facilisi.", "Ut fringilla.", "Suspendisse potenti.", "Nunc feugiat mi a tellus consequat imperdiet.", "Vestibulum sapien.", "Proin quam.", "Etiam ultrices.", "Suspendisse in justo eu magna luctus suscipit.", "Sed lectus.", "Integer euismod lacus luctus magna.", "Quisque cursus, metus vitae pharetra auctor, sem massa mattis sem, at interdum magna augue eget diam.", "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi lacinia molestie dui.", "Praesent blandit dolor.", "Sed non quam.", "In vel mi sit amet augue congue elementum.", "Morbi in ipsum sit amet pede facilisis laoreet.", "Donec lacus nunc, viverra nec, blandit vel, egestas et, augue.", "Vestibulum tincidunt malesuada tellus.", "Ut ultrices ultrices enim.", "Curabitur sit amet mauris.", "Morbi in dui quis est pulvinar ullamcorper."]

n = 1
5.times do
  cat = Category.new(
    parent_id: nil,
    name: "#{category_name.sample} #{rand(999)}"
  )
  cat.image.attach(io: File.open("db/images/#{rand(1..18)}.png"), filename: 'catpic.png')
  cat.save!
end

root_categories = Category.roots
indirects_categories = []

rand(3..5).times do
  root_categories.each do |root_category|
    child = Category.new(
      parent_id: root_category.id,
      name: "#{category_name.sample} #{rand(999)}"
    )
    child.image.attach(io: File.open("db/images/#{rand(1..18)}.png"), filename: 'catpic.png')
    child.save!

    rand(1..5).times do
      grand_child = Category.new(
        parent_id: child.id,
        name: "#{category_name.sample} #{rand(999)}"
      )
      grand_child.image.attach(io: File.open("db/images/#{rand(1..18)}.png"), filename: 'catpic.png')
      grand_child.save!
      indirects_categories << grand_child
    end
  end
end

indirects_categories.each do |create_work|
  rand(2..5).times do
    work = Work.new(
      category_id: create_work.id,
      name: "#{category_name.sample} #{rand(999)}"
    )
    work.image.attach(io: File.open("db/images/#{rand(1..18)}.png"), filename: 'catpic.png')
    rand(3..6).times do
      work.images.attach(io: File.open("db/images/#{rand(1..18)}.png"), filename: 'catpic.png')
    end
    work.save!
  end
end
