class CreateWorks < ActiveRecord::Migration[6.0]
  def change
    create_table :works do |t|
      t.string :name
      t.string :position
      t.bigint :category_id, index: true
      t.string :slug, index: { unique: true }

      t.timestamps
    end
  end
end
