class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :categories do |t|
      t.string  :name
      t.string  :position
      t.string  :ancestry, index: true
      t.string  :slug, index: { unique: true }

      t.timestamps
    end
  end
end
